require essioc
require vac_ctrl_tcp350,1.5.1

iocshLoad("${essioc_DIR}/common_config.iocsh")

epicsEnvSet("DEVICE_NAME", "Spk-030LWU:Vac-VEPT-03100")
epicsEnvSet("MOXA_HOSTNAME", "moxa-vac-spk-20-u006.tn.esss.lu.se")
epicsEnvSet("MOXA_PORT", "4002")

iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = $(DEVICE_NAME), IPADDR = $(MOXA_HOSTNAME), PORT = $(MOXA_PORT)")

iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = Spk-030LWU:Vac-VPT-03100, CONTROLLERNAME = $(DEVICE_NAME)")
